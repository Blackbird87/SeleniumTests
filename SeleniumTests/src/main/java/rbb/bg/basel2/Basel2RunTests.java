package rbb.bg.basel2;

import java.io.InputStream;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

import rbb.bg.generalfunctionalities.ReadPRopertiesFile;
import rbb.bg.generalfunctionalities.SeleniumTest;

public class Basel2RunTests extends SeleniumTest{
	
	private static final Logger LOG = LogManager.getLogger(Basel2RunTests.class);
	public static final String AUTO_IT_LOG_IN_DETAILS =  "AutoIT/handleAuthenticationWindow.exe" ; 
	public static final String DRIVER_TYPE = "webdriver.ie.driver" ;
	public static final String DRIVER_LOCATION = "IEDriver/IEDriverServer.exe";
	public static final String URL_ADDRESS = "http://localhost:8090/basel2";
	public static final String PROPERTIES_FILE_LOCATION = "TestToRun/Basel2Tests.properties";
	
	Properties prop = new Properties();
	Basel2ChooseTests basel2TestsToRun = null ; 
	InputStream input = null;

	
	/**
	 * Reads the property file and decides which tests to run
	 */
	@Test(groups="Basel2Tests")
	public void runTests()
	{
		ReadPRopertiesFile reafFile = null; 
		try
		{
			reafFile = new ReadPRopertiesFile();
			basel2TestsToRun = new Basel2ChooseTests();
			basel2TestsToRun.chooseWhichTestsToRun(reafFile.readFile(PROPERTIES_FILE_LOCATION));
		}
		catch(Exception e)
		{
			LOG.error("allTests method " , e);
		}		
	}
	
	
	@Test(groups="RBBLookUp")
	public void runTests2()
	{
		ReadPRopertiesFile reafFile = null; 
		try
		{
			System.out.println("RBBLookUp Test1");
			System.out.println("RBBLookUp Test2");
			System.out.println("RBBLookUp Test3");
			
		}
		catch(Exception e)
		{
			LOG.error("allTests method " , e);
		}		
	}
	
}
