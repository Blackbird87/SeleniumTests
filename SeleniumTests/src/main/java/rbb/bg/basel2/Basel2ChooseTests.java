package rbb.bg.basel2;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import rbb.bg.generalfunctionalities.SeleniumTest;

public class Basel2ChooseTests extends SeleniumTest{
	
	private static final Logger LOG = LogManager.getLogger(Basel2ChooseTests.class);
	
	
	/**
	 * Based on the input in the .properties file, decides which Tests to trigger
	 * @param listTest
	 */
	public void chooseWhichTestsToRun(List<String> listTest)
	{
		
		try
		{
		
		if(listTest.contains("1"))
		{
			performTest1();
		}
		if(listTest.contains("2"))
		{
			performTest2();
		}
		if(listTest.contains("3"))
		{
			performTest3();
		}
		
		
		}
		catch(Exception e)
		{
			LOG.error("chooseWhichTestsToRun method " , e);
		}
		finally
		{
			
		}
	}

}
