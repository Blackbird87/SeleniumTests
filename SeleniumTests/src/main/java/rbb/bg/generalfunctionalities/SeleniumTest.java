package rbb.bg.generalfunctionalities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class SeleniumTest {

	private static final Logger LOG = LogManager.getLogger(SeleniumTest.class);

		
	/**Checks if Login to the specified URL is possible.Opens the browser , perform a log-in to the pop up security window, closes the window
	 * Login to Basel2 Application
	 */
	public void logIn(String autoITLogInDetails , String driverType , String driverLocation , String urlAddress)
	{
		WebDriver driver = null ;
		try {
			Runtime.getRuntime().exec(autoITLogInDetails);
			System.setProperty(driverType, driverLocation);
			driver = new InternetExplorerDriver();
			driver.get(urlAddress);
			Thread.sleep(2000);
			
			LOG.info("Log-in successful to Basel2");

		} catch (Exception e) {
			LOG.error("testJenkins method, error details : " + e);
		} finally {
			if(driver!=null)
				driver.quit();
		}
	}
	
	public void performTest1()
	{
		LOG.info("Performs Test1");
	}
	
	public void performTest2()
	{
		LOG.info("Performs Test2");
	}
	
	public void performTest3()
	{
		LOG.info("Performs Test3");
	}

}
