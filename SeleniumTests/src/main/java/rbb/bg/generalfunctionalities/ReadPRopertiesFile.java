package rbb.bg.generalfunctionalities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ReadPRopertiesFile {
	
	
	private static final Logger LOG = LogManager.getLogger(ReadPRopertiesFile.class);
	Properties prop = new Properties();
	InputStream input = null;
	
	
	/**
	 * Reads the property file with all tests that will be performed
	 * @param pathToFile
	 * @return String[] with all tests
	 */
	public List<String> readFile(String pathToFile)
	{
		String testsToRun[] = null;
		
		try
		{
		input = new FileInputStream("TestToRun/Basel2Tests.properties");
		LOG.info("Basel2Tests.properties file successfully loaded");
		prop.load(input);
		String allTestsFromPropertiesFile = (String)prop.getProperty("TestsToRun");
		testsToRun = allTestsFromPropertiesFile.split(";");
		
		}catch(IOException e)
		{
			LOG.error("readFile method " , e);
		}
		finally
		{
			try {
				input.close();
			} catch (IOException e) {
				LOG.error("readFile method , failed to close file with " , e);
			}
		}
		
		return Arrays.asList(testsToRun);
	}

}
